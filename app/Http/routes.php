<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
//Vault Routes
Route::group(array('prefix' => 'vault'), function() {
    
    Route::any('adminlogin', "VaultController@adminLogin");
    Route::any('adminlogin', "VaultController@vendorRegister");
    Route::get('logout', "VaultController@logout");	
    
});


Route::get('/', function () {
    return view('index');
});

Route::get('/admindashboard', function () {
    return view('admin.admindashboard');
});

Route::any('/contact', function () {
    return view('contact');
});

Route::any('/faq', function () {
    return view('faq');
});

Route::any('/about', function () {
    return view('about');
});

