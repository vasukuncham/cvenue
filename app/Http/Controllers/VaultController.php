<?php

namespace App\Http\Controllers;

use Session;
use Auth;
use App\Clients;
use Hash;
use Response;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;

use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class VaultController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
        
    public function adminLogin(Request $request){
        $inputs = $request->all();
        if(Session::get('email') && (Session::get('userType')=='admin')){
            return Redirect::to('/admindashboard');
        }else{ 
            if ( isset($inputs['email']) &&  isset($inputs['password']) && Auth::attempt(array('email' => $inputs['email'], 'password' => $inputs['password']))){
                $user_data=User::find(Auth::id());
                Session::put('userId', $user_data->id);
                Session::put('userName', $user_data->name);
                Session::put('email', $user_data->email);
                Session::put('mobile', $user_data->mobile);
                Session::put('userType', $user_data->user_type);
                Session::put('userStatus', $user_data->status);
               return view('/admin/admindashboard');
            }else{
                
                return view('/vault/adminlogin');
            }
        }
    }

    public function logout(){
        Session::flush();
        return Redirect::action('VaultController@adminLogin');   
    }

    public function vendorRegister(){
        
    }


    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
