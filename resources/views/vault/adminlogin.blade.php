<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Register</title>
        <link rel="shortcut icon" type="image/x-icon" href="" />
        <meta charset="utf-8">
        <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <!-- CSS -->
        <link rel="stylesheet" href="{{url()}}/assets/css/bootstrap.min.css">
        <!-- JS -->
        <script src="{{url()}}/assets/js/jquery.min.js"></script>
        <script src="{{url()}}/assets/js/bootstrap.min.js"></script>
    </head>
    <body class="container-fluid">
        
        <div class="row">
            <div class=" col-lg-4 col-md-4  col-sm-8 col-xs-8 login-box center-block text-center" 
                style=" margin-top: 10%; float: none !important;" >
                <a href="{{url()}}/">
                <img src="{{url()}}/assets/images/logo.png" style="background: #000"></img></a>
            <br>
            <br><br>
            
            <form action="{{url()}}/vault/adminlogin" id="adminLoginForm" method="POST">
                <input type="email" class="form-control" placeholder="e-Mail" name="email" required />
                <br>
                {{ csrf_field() }}
                <input type="password" class="form-control" placeholder="Password" name="password" required />
                <br>
                <button  type="submit" class=" btn btn-warning form-control"> Login</button>
            </form>
            
            
            <br><br>
            <small>&copy; 2017 Celebrations Venue. All Rights Reserved</small>
            </div> 
        </div>
    </body>
    
</html>