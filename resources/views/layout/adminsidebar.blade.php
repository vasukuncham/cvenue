 <!-- main sidebar -->
    <aside id="sidebar_main">
        <div class="sidebar_main_header" style="background: #000">
            <div class="sidebar_logo">
                <a href="{{url()}}/admindashboard" class="sSidebar_hide">
                    <img src="{{url()}}/assets/images/logo.png" alt="" height="87" width="200" style="margin-top:15px;"/> 
                </a>
                <a href="#" class="sSidebar_show">
                </a>
            </div>
        </div>
        <div class="menu_section">
            <ul>
                <li id="ADDCOMPANY" title="AddCompany">
                   <a href="#">
                        <span class="menu_icon"><i class="material-icons">&#xE8D2;</i></span>
                        <span class="menu_title">Add Company</span>
                    </a>
                </li>
                <li id="LOGOUT" title="Logout">
                   <a href="{{url()}}/vault/logout">
                        <span class="menu_icon"><i class="material-icons">&#xE8D2;</i></span>
                        <span class="menu_title">Logout</span>
                    </a>
                </li>
                
                
            </ul>
        </div>
    </aside><!-- main sidebar end -->
   