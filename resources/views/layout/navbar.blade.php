<div class="collapse" id="searcharea">
        <!-- top search -->
        <div class="input-group">
            <input type="text" class="form-control" placeholder="Search for...">
            <span class="input-group-btn">
              <button class="btn btn-primary" type="button">Search</button>
          </span>
        </div>
    </div>
    <!-- /.top search -->
    <div class="top-bar">
        <div class="container">
            <div class="row">
                <div class="col-md-6 top-message">
                    <p>Welcome to Celebrations Vendor.</p>
                </div>
                <div class="col-md-6 top-links">
                    <ul class="listnone">                       
                         <li><div class="dropdown">
                            <a class="dropdown-toggle" type="button" data-toggle="dropdown" style="cursor: pointer;">Log in
                            <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                              <div style="padding: 15px; width: 250px"> 
                                <form class="form-horizontal" action="{{url()}}/vault/adminlogin" method="POST">
                                  <div class="form-group">
                                    <div class="col-sm-12">
                                      <input type="email" class="form-control" style="height: 34px;" name="email" id="email" placeholder="Enter email" required>
                                    </div>
                                  </div>
                                  {{ csrf_field() }}
                                  <div class="form-group">
                                    <div class="col-sm-12">
                                      <input type="password" style="height: 34px;" class="form-control" name="password" id="pwd" placeholder="Enter password" required>
                                    </div>
                                  </div>
                                  <div class="form-group"> 
                                    <div class="col-sm-offset-3 col-sm-12">
                                      <button type="submit" class="btn btn-primary" style="height: 40px;">Submit</button>
                                    </div>
                                  </div>
                                </form>
                              </div>
                            </ul>
                          </div>
                        </li>
                        <li><a href="{{url()}}/faq"> Help </a></li>
                        <li><a href="pricing-plan.html">Pricing</a></li>
                        <li><a href="signup-vendor.html">Are you vendor?</a></li>  
                        <li>
                            <a role="button" data-toggle="collapse" href="#searcharea" aria-expanded="false" aria-controls="searcharea"> <i class="fa fa-search"></i> </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="header">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-12 logo">
                    <div class="navbar-brand">
                        <a href="{{url()}}/">
                        <!--<img src="images/logo.png" alt="Wedding Vendors" class="img-responsive">-->
                        <p style="color: #fff; padding-top: 10px; font-size: 30px; font-weight: normal;">Celebrations Venue</p>
                        </a>
                    </div>
                </div>
                <div class="col-md-9 col-sm-12">
                    <div class="navigation" id="navigation">
                        <ul>
                            <li class="active"><a href="{{url()}}/">Home</a>
                                <!--<ul>
                                    <li><a href="index-2.html" title="Home" class="animsition-link">Home</a></li>
                                    <li><a href="index-3.html" title="Home v.2" class="animsition-link">Home v.2</a></li>
                                </ul>-->
                            </li>
                            <li><a href="{{url()}}/about">About us</a></li>
                            <!--<li><a href="#">Listing</a>
                                <ul>
                                    <li><a href="vendor-listing-row-map.html" title="Home" class="animsition-link">List / Half Map</a></li>
                                    <li><a href="vendor-listing-sidebar.html" title="Home" class="animsition-link">List / Sidebar Left</a></li>
                                    <li><a href="vendor-listing-no-sidebar.html" title="Home" class="animsition-link">List / No Sidebar</a></li>
                                    <li><a href="vendor-listing-top-map.html" title="Home" class="animsition-link">Top Map / List</a></li>
                                    <li><a href="vendor-list-4-colmun.html" title="Home" class="animsition-link">4 Column List</a></li>
                                    <li><a href="vendor-list-3-colmun.html" title="Home" class="animsition-link">3 Column List</a></li>
                                    <li><a href="vendor-list-horizontal.html" title="Home" class="animsition-link">Horizontal List </a></li>
                                    <li><a href="vendor-list-new.html" title="Home" class="animsition-link">List Variations </a></li>
                                    <li><a href="vendor-listing-bubba.html">Bubba Style Listing</a></li>
                                    <li><a href="vendor-listing-ocean.html">Ocean Style Listing</a></li>
                                </ul>
                            </li>-->
                            <li><a href="vendor-details.html">Vendor</a>
                                <ul>
                                    <li><a href="vendor-details.html">Vendor Simple</a></li>
                                    <li><a href="vendor-details-tabbed.html">Vendor Tabbed</a></li>
                                    <li><a href="vendor-profile.html">Vendor Profile</a></li>
                                </ul>
                            </li>
                            <!--<li><a href="venue-listing.html" title="Home" class="animsition-link">Suppliers</a>
                                <ul>
                                    <li><a href="venue-listing.html">Venue List</a></li>
                                    <li><a href="photography-listing.html">Photographer List</a></li>
                                    <li><a href="dresses-listing.html">Dresses List</a></li>
                                    <li><a href="florist-listing.html">Florist List</a></li>
                                    <li><a href="videography-listing.html">Videography List</a></li>
                                    <li><a href="cake-listing.html">Cake List</a></li>
                                    <li><a href="music-listing.html">Music List</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Planning Tools</a>
                                <ul>
                                    <li><a href="planning-to-do.html">To Do List</a></li>
                                    <li><a href="planning-budget.html">Budget Planner</a></li>
                                    <li><a href="planning-table-arrangement.html">Seating Planner</a></li>
                                    <li><a href="planning-guest-list.html">Guest List</a></li>
                                    <li><a href="couple-landing-page.html">Couple Signup (LP)</a></li>
                                    <li><a href="couple-dashboard.html">Couple Admin</a></li>
                                    <li><a href="dashboard-vendor.html">Vendor Admin</a></li>
                                </ul>
                            </li>-->
                            <li><a href="#">Features</a>
                                <ul>
                                    <!--<li><a href="#">Blog</a>
                                        <ul>
                                            <li><a href="blog.html">Blog Listing</a></li>
                                            <li><a href="blog-single.html">Blog Single</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="pricing-plan.html">Pricing Table</a></li>
                                    <li><a href="404-error.html">404 Error</a></li>
                                    <li><a href="#">Shortcodes</a>
                                        <ul>
                                            <li><a href="shortcode-columns.html">Column</a></li>
                                            <li><a href="shortcode-accordion.html">Accordion</a></li>
                                            <li><a href="shortcode-tabs.html">Tabs</a></li>
                                            <li><a href="shortcode-pagination.html">Paginations</a></li>
                                            <li><a href="shortcode-typography.html">Typography</a></li>
                                            <li><a href="shortcode-accordion.html">Accordion</a></li>
                                            <li><a href="shordcods-alerts.html">Alert</a></li>
                                        </ul>
                                    </li>-->
                                    <li><a href="{{url()}}/faq">FAQ's</a></li>
                                </ul>
                            </li>
                            <li><a href="{{url()}}/contact">Contact us</a></li>
                            <!--<li><a href="#">Real Weddings</a>
                                <ul>
                                    <li><a href="real-wedding-listing.html">Listing</a></li>
                                    <li><a href="real-wedding-single.html">Real Wedding Single</a></li>
                                </ul>
                            </li>-->
                        </ul>
                    </div>
                
                </div>
            </div>
        </div>
    </div>