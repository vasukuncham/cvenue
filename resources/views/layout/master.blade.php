<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Wedding Vendor | Find The Best Wedding Vendors</title>
    <!-- Bootstrap -->
    <link href="{{url()}}/assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Template style.css -->
    <link rel="stylesheet" type="text/css" href="{{url()}}/assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="{{url()}}/assets/css/style1.css">
    <link rel="stylesheet" type="text/css" href="{{url()}}/assets/css/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="{{url()}}/assets/css/owl.theme.css">
    <link rel="stylesheet" type="text/css" href="{{url()}}/assets/css/owl.transitions.css">
    <!-- Font used in template -->
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700|Roboto:400,400italic,500,500italic,700,700italic,300italic,300' rel='stylesheet' type='text/css'>
    <!--font awesome icon -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- favicon icon -->
    <link rel="shortcut icon" href="{{url()}}/assets/images/favicon.ico" type="image/x-icon">
    @yield('libraryCSS')
</head>

<div id="feedback">
    <a href="#" data-toggle="modal" data-target="#registerVendor">Vendors Reg</a>
</div>

<div class="modal fade in" id="registerVendor" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Create My Account</h4>
                    </div>
                    <div class="well-box">
                        <form id="vendorsForm" action="{{url()}}/vault/vendorRegister" method="POST">
                            <div class="form-group">
                                <input id="brand" name="brand" type="text" placeholder="Brand Name" class="form-control input-sm" required="">
                            </div>
                            <div class="form-group">
                                <input id="email" name="email" type="email" placeholder="E-Mail" class="form-control input-md" required="">
                            </div>
                            <div class="form-group">
                                <input id="phone" name="phone" type="number" placeholder="Phone" class="form-control input-md" required="">
                            </div>
                            <div class="form-group">
                                <input id="password" name="password" type="password" placeholder="Password" class="form-control input-md" required="">
                                <span id="passDiv"></span>
                            </div>
                            <div class="form-group">
                                <input id="confirmPassword" name="confirmPassword" type="password" placeholder="Confirm Password" class="form-control input-sm" required="">
                                <span id="confirmDiv"></span>
                            </div>
                            <div class="form-group">
                                <select id="category" name="category" class="form-control selectpicker">
                                    <option value="Couple">Couple</option>
                                    <option value="Vendor">Vendor</option>
                                    <option value="Advertisement">Advertisement</option>
                                    <option value="Suggestion">Suggestion</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <button id="submit" name="submit" class="btn btn-primary btn-sm">Submit</button>
                            </div>
                        </form>
                </div>
            </div>
        </div>
    </div>
</div>

    <body>
    <!-- including nav bar -->
    @include('layout.navbar')
    @yield('content')


    @yield('libraryJS')
    @include('layout.footer')
    <!-- /. Tiny Footer -->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="{{url()}}/assets/js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{url()}}/assets/js/bootstrap.min.js"></script>
    <!-- Flex Nav Script -->
    <script src="{{url()}}/assets/js/jquery.flexnav.js" type="text/javascript"></script>
    <script src="{{url()}}/assets/js/navigation.js"></script>
    <!-- slider -->
    <script src="{{url()}}/assets/js/owl.carousel.min.js"></script>
    <script type="text/javascript" src="{{url()}}/assets/js/slider.js"></script>
    <!-- testimonial -->
    <script type="text/javascript" src="{{url()}}/assets/js/testimonial.js"></script>
    <!-- sticky header -->
    <script src="{{url()}}/assets/js/jquery.sticky.js"></script>
    <script src="{{url()}}/assets/js/header-sticky.js"></script>
    <script type="text/javascript" src="http://arrow.scrolltotop.com/arrow30.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $(".scroll").click(function(event){
                event.preventDefault();
                $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
            });
        });
        $('#vendorsForm').on('change', '#confirmPassword', function(){
            var confirm = $('#confirmPassword').val();
            var pass = $('#password').val();
            if (pass != '' && typeof pass != 'undefined') {
                if (pass != confirm) {
                    $('#confirmDiv').html("<small style='color: red'>Password and confirm password not matched</small>");
                    $('#confirmDiv').focus();
                }else{
                    $('#confirmDiv').html("");
                    $('#passDiv').html("");
                }
            }else{
                $('#passDiv').html("<small style='color: red'>Please fill Password First</small>");
                $('#password').focus();
                $('#confirmPassword').val('');
            }
        });
    </script>
    </body>
</html>